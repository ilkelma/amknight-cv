SECRETS_FILE := $(shell ${SECRETS_FILE:-secrets.yaml})

.PHONY: inject-secrets
inject-secrets: clean
	yq merge -x ${SECRETS_FILE} base.yaml > tmpBase.yaml

.PHONY: base-json
base-json: inject-secrets
	yq -j read --prettyPrint tmpBase.yaml > resume.json

.PHONY: primary
primary: base-json
	resume export --theme stackoverflow --format html outputs/primary.html
	resume export --theme stackoverflow --format pdf outputs/primary.pdf

.PHONY: all
all: primary
	./build-all.sh

.PHONY: clean
clean:
	rm -f tmpBase.yaml resume.json outputs/*.html outputs/*.pdf

.PHONY: public
public: all
	cp outputs/primary.html public/index.html
	cp outputs/*.html public/

.PHONY: ci
ci: public
	cat public/index.html
