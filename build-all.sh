for jobFile in $(ls jobs/*.yaml)
do
    job=$(basename $jobFile | cut -d. -f1)
    yq merge -x tmpBase.yaml $jobFile > tmp$job.yaml
	yq -j read --prettyPrint tmp$job.yaml > resume.json
	resume export --theme stackoverflow --format html outputs/$job.html
    if [[ "${PROCESS_PDF:-true}" = true ]]; then
	    resume export --theme stackoverflow --format pdf outputs/$job.pdf
    fi
    rm tmp$job.yaml
done